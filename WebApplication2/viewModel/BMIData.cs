﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.viewModel
{
    public class BMIData
    {
        [Display(Name = "體重")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(30, 150, ErrorMessage = "請輸入30-150的數值")]

 
        public float? w{ get; set; }

        [Display(Name = "身高")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(30, 150, ErrorMessage = "請輸入30-150的數值")]
        public float? h { get; set; }
        public float? result { get; set; }
        public string level { get; set; }

    }
}