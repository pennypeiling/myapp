﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["aaa"] = "111";
            ViewBag.NAME = "PENNY";
            ViewBag.A = 1;
            ViewBag.B = 2;
            return View();
        }

        public ActionResult Html()
        {
            return View();
        }
        public ActionResult Htmlhelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }
    }
}