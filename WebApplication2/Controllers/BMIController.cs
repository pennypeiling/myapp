﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.viewModel;

namespace WebApplication2.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index() {
            return View(new BMIData());
        }
            [HttpPost]
            // GET: BMI
            public ActionResult Index(BMIData data)
        {

            //if (data.h < 50 || data.h > 200) {
            //    ViewBag.hError = "身高請輸入50~200的數值";
            //}
            //if (data.w < 30 || data.w > 150)
            //{
            //    ViewBag.wError = "體重請輸入30~150的數值";
            //}
            if (ModelState.IsValid)
            {
                var m = data.h.Value / 100;
                var result = data.w / (m*m);
                var level = "";
                if (result < 18.5) {
                    level = "體重過輕";
                }
                else if (18.5 <= result && result < 24)
                {
                    level = "正常範圍";
                }
                else if (24 <= result && result < 27) {
                    level = "過重";
                }
                else if (27 <= result && result < 30){
                    level = "輕度肥胖";
                }
                else if (30 <= result && result < 35){
                    level = "中度肥胖";
                }
                else if (result>=35){
                    level = "重度肥胖";
                }
                //ViewBag.h = h;
                //ViewBag.w = w;
                data.result = result;
                data.level = level;
            }
            return View(data);
        }
    }
}